package com.sam.hello_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * 
 * Spring Cloud Feign 通过@EnableFeignClients来开启spring cloud feign的支持功能 不仅包含Spring
 * Cloud ribbon负责均衡功能，也包含Spring Cloud Hystrix服务容错功能，还提供了一种声明式的Web服务客户端定义方式。
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class FeignApp {
	// @Bean 应用在方法上，用来将方法返回值设为为bean
	@Bean
	@LoadBalanced // @LoadBalanced实现负载均衡
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(FeignApp.class, args);
	}
}
